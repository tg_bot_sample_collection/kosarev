import os
from aiogram import Bot, Dispatcher, executor, types
import asyncio
import aiohttp
import re
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import matplotlib.cm as cmap
import json

bot = Bot('5330174272:AAFJb7GVArtFR4eBMyWmN0jCjbYyiBhdBeg')
dp = Dispatcher(bot)
strings = {
     '2': '100000100001101000001',
     '4': '101110100101001011101',
     '5': '101110100000101011101',
     '8': '000000000001000000000',
     '12': '111011011110111001100',
     '15': '111111101111111001100',
     '19': '101110101001010100111'
}

ss = []

async def get_html():
    async with aiohttp.ClientSession() as session:
        async with session.get('http://python.org') as resp:
            res = await resp.text()
            print(re.find('right', res))
            return await resp.read()

async def congrats(message: types.Message):
    await bot.send_message(message.chat.id, 'Ура! Ну это 5, я считаю')

with open('list_dict.txt') as f:
    data = f.read()
    code_dict = json.loads(data)

def write_code():
    with open('list_dict.txt', 'w') as convert_file:
        convert_file.write(json.dumps(code_dict))

@dp.message_handler(commands=["start"])
async def start(message: types.Message):
    if f'code{message.chat.id}' not in code_dict.keys():
        with open('broke_code.txt') as bc:
            data = bc.read()
        code_dict[f'code{message.chat.id}'] = json.loads(data)
        for i in range(7):
             ss.append(False)
    else:
        for i in list(strings.keys()):
            ss.append(sum(code_dict[f'code{message.chat.id}'][int(i)-1])>0)
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1=types.KeyboardButton(text="2️⃣")
    item2=types.KeyboardButton(text="4️⃣")
    item3=types.KeyboardButton(text="5️⃣")
    item4=types.KeyboardButton(text="8️⃣")
    item5=types.KeyboardButton(text="1️⃣2️⃣")
    item6=types.KeyboardButton(text="1️⃣5️⃣")
    item7=types.KeyboardButton(text="1️⃣9️⃣")
    item8=types.KeyboardButton(text="🔎")
    markup.add(item1, item2, item3, item4, item5, item6, item7, item8)
    await bot.send_photo(message.chat.id, open('broke_qr.png', 'rb'))
    await message.reply('Наш QR-код сломался! Необходимо его собрать', reply_markup=markup)
    await get_html()

@dp.message_handler(lambda message: message.text == "🔎")
async def get_code(message: types.Message):
    plt.figure()
    plt.imshow(code_dict[f'code{message.from_user.id}'], cmap=cmap.binary)
    plt.axis('off')
    if os.path.exists(f'code{message.from_user.id}.png'):
        os.remove(f'code{message.from_user.id}.png')
    plt.savefig(f'code{message.from_user.id}.png')
    await bot.send_photo(message.chat.id, open(f'code{message.from_user.id}.png', 'rb'))

@dp.message_handler(lambda message: message.text == "2️⃣")
async def string2(message: types.Message):
    await bot.send_message(message.chat.id, 'Эта строка рассыпалась на три массива: \narray1 = [1, 0, 0, 0, 0, 0, 1]\narray2 = [0, 0, 0, 0, 1, 1, 0]\narray3 = [1, 0, 0, 0, 0, 0, 1]')
        
@dp.message_handler(lambda message: message.text == "4️⃣")
async def string4(message: types.Message):
    await bot.send_message(message.chat.id, 'В эту строку попали буквы: \n101g1l1010vf0101F0G0aU10z1v1c1k01')
        
@dp.message_handler(lambda message: message.text == "5️⃣")
async def string5(message: types.Message):
    await bot.send_message(message.chat.id, 'Эта строка распалась на множители: \n79\n461\n2776300831986079')

@dp.message_handler(lambda message: message.text == "8️⃣")
async def string8(message: types.Message):
    await bot.send_message(message.chat.id, 'Эта строка оказалась в десятичной сс: \n512\n(помни, что в итоговой строке должен быть 21 символ)')

@dp.message_handler(lambda message: message.text == "1️⃣2️⃣")
async def string12(message: types.Message):
    await bot.send_message(message.chat.id, 'В этой строке чётные числа заменили все нули, а нечётные - все единицы: \n317215077196113809142')

@dp.message_handler(lambda message: message.text == "1️⃣5️⃣")
async def string15(message: types.Message):
    await bot.send_message(message.chat.id, 'В этой строке буквы "l" и "O" заменили 1 и 0 соответсвенно: \nl1l1ll1O1ll1l1lO0l1OO')

@dp.message_handler(lambda message: message.text == "1️⃣9️⃣")
async def string19(message: types.Message):
    await bot.send_message(message.chat.id, 'Эта строка оказалась закодирована символами: \n]%\'\n(помни, что в итоговой строке должен быть 21 символ)')

@dp.message_handler()
async def handle_text(message: types.Message):
    if message.text == strings['2']:
            code_dict[f'code{message.from_user.id}'][1] = [int(char) for char in message.text]
            write_code()
            ss[0] = True
            await bot.send_message(message.chat.id, 'Вы нашли строчку 2!')
            if (sum(ss)==7):
                await bot.send_message(message.chat.id, 'Вы нашли все строки! Сканируй и отправляй кодовое слово')
    elif message.text == strings['4']:
            code_dict[f'code{message.from_user.id}'][3] = [int(char) for char in message.text]
            write_code()
            ss[1] = True
            await bot.send_message(message.chat.id, 'Вы нашли строчку 4!')
            if (sum(ss)==7):
                await bot.send_message(message.chat.id, 'Вы нашли все строки! Сканируй и отправляй кодовое слово')
    elif message.text == strings['5']:
            code_dict[f'code{message.from_user.id}'][4] = [int(char) for char in message.text]
            write_code()
            ss[2] = True
            await bot.send_message(message.chat.id, 'Вы нашли строчку 5!')
            if (sum(ss)==7):
                await bot.send_message(message.chat.id, 'Вы нашли все строки! Сканируй и отправляй кодовое слово')
    elif message.text == strings['8']:
            code_dict[f'code{message.from_user.id}'][7] = [int(char) for char in message.text]
            write_code()
            ss[3] = True
            await bot.send_message(message.chat.id, 'Вы нашли строчку 8!')
            if (sum(ss)==7):
                await bot.send_message(message.chat.id, 'Вы нашли все строки! Сканируй и отправляй кодовое слово')
    elif message.text == strings['12']:
            code_dict[f'code{message.from_user.id}'][11] = [int(char) for char in message.text]
            write_code()
            ss[4] = True
            await bot.send_message(message.chat.id, 'Вы нашли строчку 12!')
            if (sum(ss)==7):
                await bot.send_message(message.chat.id, 'Вы нашли все строки! Сканируй и отправляй кодовое слово')
    elif message.text == strings['15']:
            code_dict[f'code{message.from_user.id}'][14] = [int(char) for char in message.text]
            write_code()
            ss[5] = True
            await bot.send_message(message.chat.id, 'Вы нашли строчку 15!')
            if (sum(ss)==7):
                await bot.send_message(message.chat.id, 'Вы нашли все строки! Сканируй и отправляй кодовое слово')
    elif message.text == strings['19']:
            code_dict[f'code{message.from_user.id}'][18] = [int(char) for char in message.text]
            write_code()
            ss[6] = True
            await bot.send_message(message.chat.id, 'Вы нашли строчку 19!')
            if (sum(ss)==7):
                await bot.send_message(message.chat.id, 'Вы нашли все строки! Сканируй и отправляй кодовое слово')
    elif message.text == 'Дерево':
            if (sum(ss)==7):
                await congrats(message)
            else:
                await bot.send_message(message.chat.id, 'Кажется, вы нашли не все строки')
    else:
            await bot.send_message(message.chat.id, 'Я ничего не понял, попробуй ещё раз')

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
